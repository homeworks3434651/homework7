package org.homework7_variant2_with_stream;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        // Family 1
        Family family1 = new Family(new Man("John", "Doe", 1970), null);
        family1.getFather().setIQ(120);
        DayOfWeek monday = DayOfWeek.MONDAY;
        Map<DayOfWeek, List<String>> schedule = new HashMap<>();

        // Create a new ArrayList from the result of Arrays.asList
        List<String> mondayActivities = new ArrayList<>(Arrays.asList("Writing newspapers", "Listening to the radio"));
        schedule.put(monday, mondayActivities);

        // Add new activities to the existing list for MONDAY
        schedule.get(monday).addAll(Arrays.asList("Fishing", "Car racing"));
        System.out.println(schedule);

        family1.getFather().setSchedule(schedule);
        System.out.println(family1.getFather().getSchedule());

        List<String> activityForMondayFather = DayOfWeek.getActivitiesForDay(monday, schedule);
        System.out.println("Father MONDAY Activity: " + activityForMondayFather);

        System.out.println(family1.getFather());

        Woman mother_family1 = new Woman("Jane", "Doe", 1975, 128, family1,
                monday.name(), "Cinema");
        mother_family1.getSchedule().get(monday).add("Сooking");
        mother_family1.setActivitiesForDays(monday, "Dancing");

        System.out.println("Mother MONDAY Activity: " + family1.getMother().getSchedule().get(monday));
        System.out.println("Mother FRIDAY Activity: " + DayOfWeek.getActivitiesForDay(DayOfWeek.FRIDAY, mother_family1.getSchedule()));

        Dog pet_family1 = new Dog();
        pet_family1.setNickname("Buddy");
        pet_family1.setAge(4);
        pet_family1.setTrickLevel(55);
        Set<String> pet_family1_habits = new HashSet<>(Arrays.asList("eat", "drink", "sleep"));
        pet_family1.setHabits(pet_family1_habits);
        family1.setPets(pet_family1);

        System.out.println(family1.getFather());
        System.out.println(family1.getFather().greetPet());
        family1.getFather().repairCar();

        System.out.println(family1.getMother());
        System.out.println(family1.getMother().greetPet());
        System.out.println(mother_family1.greetPet());
        mother_family1.makeup();

        // Use getters
        System.out.println("\nPet in Family 1:");
        System.out.println(family1.getPets());
        System.out.println("\nHabits of pet_for_child_family1:");
        System.out.println(pet_family1.getHabits());

        pet_family1.eat();
        pet_family1.respond();
        pet_family1.foul();
        family1.getFather().feedPet(false);

        Human newChildFamily1 = family1.getMother().bornChild(null, "", family1, 0);
        family1.addChild(newChildFamily1);

        System.out.println("\nNew child in Family 1:");
        System.out.println(newChildFamily1);
        System.out.println("\nFamily 1 (with the new child):");
        System.out.println(family1);

        Human child_family1 = new Human("Alex", "Doe", 2000, 120, family1,
                DayOfWeek.FRIDAY.name(), "Chess",
                DayOfWeek.MONDAY.name(), "Soccer, Cards",
                DayOfWeek.WEDNESDAY.name(), "Reading");

        System.out.println("FRIDAY Activity for Alex: " + child_family1.getSchedule().get(DayOfWeek.FRIDAY));
        DayOfWeek.FRIDAY.setActivities(child_family1.getSchedule().get(DayOfWeek.FRIDAY));

        List<String> activityForAlexMonday = DayOfWeek.getActivitiesForDay(DayOfWeek.MONDAY, child_family1.getSchedule());
        System.out.println("MONDAY Activity for Alex: " + activityForAlexMonday);
        System.out.println(child_family1.getSchedule());

        System.out.println("\nChild in Family 1:");
        System.out.println(child_family1);

        child_family1.greetPet();
        child_family1.describePet();
        System.out.println();

        family1.addChild(child_family1);
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));

        System.out.println("\nFamily 1 (with pets and children):");
        System.out.println(family1);
        System.out.println();

        family1.deleteChild(family1.getChildren().get(0));
        family1.deleteChild(new Human("Bob", "Doe", 1995));   // I get the error message
        family1.deleteChild(new Human("Bob", "Doe", 1995));   // I get the error message
        family1.deleteChild(1);
        try {
            family1.deleteChild(family1.getChildren().get(0));
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());    // I get the exception message
        }
        family1.deleteChild(0);  // I get the error message
        try {
            family1.deleteChild(family1.getChild(10));
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());    // I get the exception message
        }

        List<Human> newChildren_family1 = Arrays.asList(
                new Human("Emily", "Doe", 2012),
                new Human("Brandon", "Doe", 2014, family1),
                new Human("Landon", "Doe", 2017)
        );

        family1.setChildren(newChildren_family1);

        // Create a second Pet for family1
        RoboCat pet_family1_second = new RoboCat("Max");
        Species.ROBO_CAT.setCanFly(true);
        Species.ROBO_CAT.setNumberOfLegs(2);
        Species.ROBO_CAT.setHasFur(true);
        pet_family1_second.setAge(3);
        pet_family1_second.setTrickLevel(40);
        pet_family1_second.setHabits(new HashSet<>(Arrays.asList("play", "fetch")));
        family1.setPets(pet_family1_second);

        family1.getChildren().get(1).feedPet(true);

        // Use getters
        System.out.println("\nPets in Family 1:");
        System.out.println(family1.getPets());
        System.out.println();

        Human lastChild = family1.getChildren().get(family1.getChildren().size() - 1);
        System.out.println("\nLast Child in Family 1:");
        System.out.println(lastChild);
        lastChild.greetPet();
        lastChild.describePet();

        System.out.println("\nFamily 1 after setting new children and the new pet:");
        System.out.println(family1);

        // Set a new father
        Human newFather_family1 = new Human("Bob", "Kennedy", 1970);
        family1.setFather(newFather_family1);  // I get the error message

        // Family 2
        Man father_family2 = new Man("Bob", "Smith", 1975);
        Woman mother_family2 = new Woman("Alice", "Smith", 1980);
        Family family2 = new Family(father_family2, mother_family2);

        mother_family2.setIQ(112);
        mother_family2.setFamily(family2);
        mother_family2.setSchedule(new HashMap<>(
                Map.of(
                        DayOfWeek.WEDNESDAY, List.of("Club"),
                        DayOfWeek.SATURDAY, List.of("Cafe")
                )
        ));
        mother_family2.addDayToSchedule(DayOfWeek.FRIDAY, "Restaurant");
        List<String> activityForMotherFriday = DayOfWeek.getActivitiesForDay(DayOfWeek.FRIDAY, mother_family2.getSchedule());
        System.out.println("FRIDAY Activity for Mother: " + activityForMotherFriday);
        List<String> activityForMotherWednesday = DayOfWeek.getActivitiesForDay(DayOfWeek.WEDNESDAY, mother_family2.getSchedule());
        System.out.println("WEDNESDAY Activity for Mother: " + activityForMotherWednesday);
        System.out.println(mother_family2.getSchedule());

        family2.getFather().setIQ(124);
        family2.getFather().setFamily(family2);
        family2.getFather().setSchedule(DayOfWeek.TUESDAY.name(), "Watching soccer");

        family2.addChild(child_family1);     //  I get the error message

        Human newChildFamily2_not_valid = family2.getMother().bornChild(null, "", family1, 0);
        family2.addChild(newChildFamily2_not_valid);    //  I get the error message

        System.out.println("\nFamily 2:");
        System.out.println(family2);

        Human newChildFamily2 = family2.getMother().bornChild(null, "", family2, 0);
        family2.addChild(newChildFamily2);

        System.out.println("\nNew child in Family 2:");
        System.out.println(newChildFamily2);

        DomesticCat pet_family2 = new DomesticCat("Whiskers");
        family2.setPets(pet_family2);

        Human child_family2 = new Human("Ryan", "Smith", 2005, family2);
        family2.addChild(child_family2);
        System.out.println("\nChild in Family 2:");
        System.out.println(child_family2);

        child_family2.setFamily(family1);  // I get the error message

        family2.addChild(new Human("Debora", "Smith", 2010));
        family2.addChild(new Human("Ruth", "Smith", 2015, 118, family2,
                DayOfWeek.THURSDAY.name(), "Aerobics", DayOfWeek.SUNDAY.name(), "Grass hockey"));

        System.out.println(family2.getChildren().get(family2.getChildren().size() - 1).getSchedule());

        pet_family2.setAge(2);
        pet_family2.setTrickLevel(60);
        System.out.println("\nPet in Family 2:");
        System.out.println(pet_family2);
        System.out.println();

        family2.getChildren().get(2).feedPet(false);

        System.out.println("\nFamily 2 (with pets and children):");
        System.out.println(family2);

        // Set a new mother
        Human newMother = new Human("Marina", "Krotova", 1980, family2);
        family2.setMother(newMother);   // I get the error message
        System.out.println();

        // Family 3
        Family family3 = new Family(new Man("Charlie", "Brown", 1990),
                new Woman("Lucy", "Brown", 1992));

        Human son_family3 = new Human("Bryan", "Brown", 2010, 120, family3,
                DayOfWeek.SATURDAY.name(), "Gym", DayOfWeek.TUESDAY.name(), "Studying");

        family3.addChild(son_family3);
        son_family3.addDayToSchedule(DayOfWeek.FRIDAY, "Hiking");

        System.out.println("Son in Family 3:");
        System.out.println(son_family3);

        // Use getters
        Human sonFamily3 = family3.getChildren().get(0);

        System.out.println("\nSon in Family 3 - IQ: " + sonFamily3.getIQ());
        System.out.println("Son in Family 3 - Schedule: " + sonFamily3.getSchedule());

        List <String> activityForTuesday = DayOfWeek.getActivitiesForDay(DayOfWeek.TUESDAY, sonFamily3.getSchedule());
        System.out.println("TUESDAY Activity for Bryan: " + activityForTuesday);
        String activityForWednesday = DayOfWeek.getActivityForDay(DayOfWeek.WEDNESDAY, sonFamily3.getSchedule());
        System.out.println("WEDNESDAY Activity for Bryan: " + activityForWednesday);

        Fish pet_family3 = new Fish("Polly", 3, 70, new HashSet<>(Arrays.asList("swimming", "exploring", "resting")));
        family3.setPets(pet_family3);

        System.out.println("Son in Family 3 - Pet: " + family3.getPets());
        pet_family3.eat();
        pet_family3.respond();
        family3.getMother().feedPet(false);

        Human daughter_family3 = new Human();

        daughter_family3.setName("Laura");
        daughter_family3.setSurname("Brown");
        daughter_family3.setYear(2006);
        daughter_family3.setIQ(100);
        daughter_family3.setFamily(family3);
        Map<DayOfWeek, List<String>> scheduleForLaura = Map.of(
                DayOfWeek.WEDNESDAY, List.of("Run"),
                DayOfWeek.SUNDAY, List.of("Dance Class"),
                DayOfWeek.MONDAY, List.of("Skating")
        );
        daughter_family3.setSchedule(scheduleForLaura);
        daughter_family3.addDayToSchedule(DayOfWeek.THURSDAY, "Party");
        System.out.println(daughter_family3.getSchedule());

        String activityForLauraMonday = DayOfWeek.getActivityForDay(DayOfWeek.MONDAY, scheduleForLaura);
        System.out.println("MONDAY Activity for Laura: " + activityForLauraMonday);

        family3.addChild(daughter_family3);
        System.out.println("\nDaughter in Family 3:");
        System.out.println(daughter_family3);

        System.out.println("\nFamily 3 (with pets and children):");
        System.out.println(family3);
        System.out.println();

        // False families
        Family false_family1 = new Family(father_family2, new Woman("Marina", "Krotova", 1980));
        System.out.println(false_family1);  // I get the error message

        Family false_family2 = new Family(new Man("Charlie", "Brown", 1990), mother_family2);
        System.out.println(false_family2);  // I get the error message
        mother_family2.bornChild(null, "", false_family2, 0);   // I get the error message

        Family false_family3 = new Family(new Man("Mike", "Jackson", 1985),
                new Woman("Mike", "Jackson", 1985));
        System.out.println(false_family3);  // I get the error message

        // Invalid family and fixing it
        Family valid_family = new Family(null, null);
        System.out.println(valid_family.getFather());
        System.out.println(valid_family.getMother());

        // Set a mother
        Woman mother_valid_family = new Woman("Irin", "Peterson", valid_family, 115);
        valid_family.setMother(mother_valid_family);
        System.out.println(valid_family.getMother());

        // Set a father
        Man father_valid_family = new Man("Ted", "Jansen", 1976);
        valid_family.setFather(father_valid_family);
        System.out.println(valid_family.getFather());

        Human newChild_valid_family = mother_valid_family.bornChild(null, "", valid_family, 0);
        valid_family.addChild(newChild_valid_family);
        System.out.println(newChild_valid_family);
        System.out.println(valid_family);

        // Create an unknown Pet
        Pet pet_valid_family = new Pet("Petryk", 3, 65, new HashSet<>(Arrays.asList("play", "fetch"))) {
            @Override
            public void respond() {

            }

            @Override
            public void eat() {

            }
        };

        valid_family.setPets(pet_valid_family);

        father_valid_family.feedPet(false);

        System.out.println("\nFamily after setting the new pet:");
        System.out.println(valid_family);
    }
}
