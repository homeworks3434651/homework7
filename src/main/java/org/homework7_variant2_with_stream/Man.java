package org.homework7_variant2_with_stream;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class Man extends Human {

    public Man() {
        super();
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, Family family) {
        super(name, surname, year, family);
    }

    public Man(String name, String surname, Family family, int iq) {
        super(name, surname);
        this.setFamily(family);
        this.setIQ(iq);
    }

    public Man(String name, String surname, int year, int iq, Family family, String... schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    public Man(String name, String surname, int year, int iq, Family family, Map<DayOfWeek, List<String>> schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    @Override
    public String greetPet() {
        if (this.getFamily() != null && this.getFamily().getPets() != null && !this.getFamily().getPets().isEmpty()) {
            return "Hello, my favorite friend " +
                    this.getFamily().getPets().stream()
                            .map(Pet::getNickname)
                            .collect(Collectors.joining("!"));
        } else {
            return "I don't have a pet.";
        }
    }

    public void repairCar() {
        System.out.println("I'm repairing the car.");
    }

    @Override
    public String toString() {
        return "Man{" +
                "name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", year=" + getYear() +
                ", iq=" + getIQ() +
                ", schedule=" + getSchedule() +
                '}';
    }
}
