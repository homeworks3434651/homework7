package org.homework7_variant2_with_stream;

import java.util.*;
import java.util.stream.Collectors;

public final class Woman extends Human implements HumanCreator {

    public Woman() {
        super();
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, Family family) {
        super(name, surname, year, family);
    }

    public Woman(String name, String surname, Family family, int iq) {
        super(name, surname);
        this.setFamily(family);
        this.setIQ(iq);
    }

    public Woman(String name, String surname, int year, int iq, Family family, String... schedule) {
        super(name, surname, year, iq, family, schedule);

        // Set the family for the mother
        if (family != null) {
            family.setMother(this);
        }
    }

    public Woman(String name, String surname, int year, int iq, Family family, Map<DayOfWeek, List<String>> schedule) {
        super(name, surname, year, iq, family, schedule);

        // Set the family for the mother
        if (family != null) {
            family.setMother(this);
        }
    }

    @Override
    public String greetPet() {
        if (this.getFamily() != null && this.getFamily().getPets() != null && !this.getFamily().getPets().isEmpty()) {
            return "Hello, I greet you " +
                    this.getFamily().getPets().stream()
                            .map(Pet::getNickname)
                            .collect(Collectors.joining("!"));
        } else {
            return "I don't have a pet.";
        }
    }

    public void makeup() {
        System.out.println("I'm applying makeup.");
    }

    public static final List<String> CHILD_NAMES = List.of("John", "Alice", "Bob", "Eva", "Charlie", "Olivia");
    public static final String[] MALE_NAMES = {"John", "Bob", "Charlie"};
    public static final String[] FEMALE_NAMES = {"Alice", "Eva", "Olivia"};

    @Override
    public Human bornChild(String childName, String fatherSurname, Family family, int averageIQ) {
        if (family == null) {
            System.out.println("Warning: Family is not specified. Setting child's family to the mother's family.");
            family = this.getFamily();
        }

        Man father = family.getFather();
        Woman mother = family.getMother();
        Human child = new Human();

        if (child == null) {
            System.out.println("Error: Child can't be null.");
            return null;
        }

        if (father == null || mother == null) {
            System.out.println("Error: Father or mother is missing in the family.");
            return null;
        }

        if (childName == null || childName.isEmpty()) {
            Random random = new Random();
            childName = CHILD_NAMES.get(random.nextInt(CHILD_NAMES.size()));
        }

        // Determine the gender randomly (50% chance for Man or Woman)
        boolean isMale = new Random().nextBoolean();

        // Create a new instance of Man or Woman based on the determined gender
        if (isMale) {
            child = new Man(childName, fatherSurname, family, averageIQ);
        } else {
            child = new Woman(childName, fatherSurname, family, averageIQ);
        }

        // Check if the name matches the gender and replace it if needed
        if (isMale && !isMaleName(childName)) {
            childName = getRandomMaleName();
            child.setName(childName);
        } else if (!isMale && isMaleName(childName)) {
            childName = getRandomFemaleName();
            child.setName(childName);
        }

        child.setSurname(father.getSurname());
        int inheritedIQ = calculateInheritedIQ(father, mother, child);
        child.setIQ(inheritedIQ);

        return child;
    }

    public int calculateInheritedIQ(Man father, Woman mother, Human child) {
        validateAndAdjustIQ(father, mother);

        int fatherIQ = father.getIQ();
        int motherIQ = mother.getIQ();

        // If the child's IQ is set, return that value
        if (child.getIQ() > 0 && child.getIQ() != 110) {
            return child.getIQ();
        }

        return (fatherIQ + motherIQ) / 2;
    }

    private void validateAndAdjustIQ(Man father, Woman mother) {
        if (father.getIQ() == 0 && mother.getIQ() == 0) {
            System.out.println("Error: At least one parent's IQ must be specified.");
            father.setIQ(110);
            mother.setIQ(110);
            return;
        }

        if (father.getIQ() == 0) {
            System.out.println("Warning: Father's IQ is not specified. Using mother's IQ for the child.");
            father.setIQ(mother.getIQ());
        }

        if (mother.getIQ() == 0) {
            System.out.println("Warning: Mother's IQ is not specified. Using father's IQ for the child.");
            mother.setIQ(father.getIQ());
        }
    }

    // Additional methods for getting random names of specific gender
    public String getRandomMaleName() {
        Random random = new Random();
        return MALE_NAMES[random.nextInt(MALE_NAMES.length)];
    }

    public String getRandomFemaleName() {
        Random random = new Random();
        return FEMALE_NAMES[random.nextInt(FEMALE_NAMES.length)];
    }

    public boolean isMaleName(String name) {
        for (String maleName : MALE_NAMES) {
            if (maleName.equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Woman{" +
                "name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", year=" + getYear() +
                ", iq=" + getIQ() +
                ", schedule=" + getSchedule() +
                '}';
    }
}
