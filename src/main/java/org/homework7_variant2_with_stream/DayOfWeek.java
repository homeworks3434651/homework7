package org.homework7_variant2_with_stream;

import java.util.*;
import java.util.stream.Collectors;

public enum DayOfWeek {
    MONDAY(new ArrayList<>()),
    TUESDAY(new ArrayList<>()),
    WEDNESDAY(new ArrayList<>()),
    THURSDAY(new ArrayList<>()),
    FRIDAY(new ArrayList<>()),
    SATURDAY(new ArrayList<>()),
    SUNDAY(new ArrayList<>());

    private List<String> activities;

    DayOfWeek(List<String> activities) {
        this.activities = activities;
    }

    public void addActivities(String activity) {
        this.activities.add(activity);
    }

    public void mergeActivities(List<String> newActivities) {
        this.activities = newActivities.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<String> getActivitiesForDay(DayOfWeek day, Map<DayOfWeek, ?> schedule) {
        return Optional.ofNullable(schedule.get(day))
                .map(activities -> {
                    if (activities instanceof List<?>) {
                        return (List<?>) activities;
                    } else if (activities instanceof String) {
                        return List.of((String) activities);
                    }
                    return Collections.emptyList();
                })
                .map(list -> list.stream()
                        .map(Object::toString)
                        .collect(Collectors.toList()))
                .orElse(List.of("No activity specified"));
    }

    public static String getActivityForDay(DayOfWeek dayOfWeek, Map<DayOfWeek, List<String>> schedule) {
        List<String> activities = schedule.getOrDefault(dayOfWeek, Collections.emptyList());

        return activities.isEmpty() ?
                "No activity specified" :
                activities.stream()
                        .collect(Collectors.joining(", ", "", ""));
    }

    public void setActivities(List<String> activities) {
        this.activities.addAll(activities);
    }
}
