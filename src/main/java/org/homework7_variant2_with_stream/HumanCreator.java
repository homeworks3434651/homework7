package org.homework7_variant2_with_stream;

public interface HumanCreator {

    Human bornChild(String childName, String fatherSurname, Family family, int averageIQ);
}
