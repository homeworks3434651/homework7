package org.homework7_variant1;

import java.util.*;

public class Family {
    private Man father;
    private Woman mother;
    private List<Human> children;
    private Set<Pet> pets;

    private boolean parentsSet = false;

    private String errorMessage;

    static {
        System.out.println("Family class is loaded.");
    }

    {
        System.out.println("A new Family object is created.");
    }

    public Family(Man father, Woman mother) {
        if (father != null && mother != null) {
            if (!checkParentTypes(father, mother)) {
                errorMessage = "Error: Father must be of type Man, and mother must be of type Woman.";
                printErrorMessage();
                return;
            }

            if (father.equals(mother)) {
                errorMessage = "Error: Father and mother cannot be the same person.";
                printErrorMessage();
                return;
            }

            if (father.getFamily() != null || mother.getFamily() != null) {
                errorMessage = "Error: One or both parents are already part of another family.";
                printErrorMessage();
                return;
            }

            this.father = father;
            this.father.setFamily(this);
            this.mother = mother;
            this.mother.setFamily(this);
            this.children = new ArrayList<>();
            this.pets = new HashSet<>();
            parentsSet = true;
        } else if (father == null && mother == null) {
            errorMessage = "Invalid family creation. Please ensure both parents are provided and not null.";
            printErrorMessage();
            this.father = null;
            this.mother = null;
            this.children = new ArrayList<>();
            parentsSet = false;
        } else if (father == null) {
            errorMessage = "Error: Father cannot be null.";
            printErrorMessage();
            this.father = null;
            this.mother = mother;
            this.mother.setFamily(this);
            this.children = new ArrayList<>();
            parentsSet = false;
        } else {
            errorMessage = "Error: Mother cannot be null.";
            printErrorMessage();
            this.father = father;
            this.father.setFamily(this);
            this.mother = null;
            this.children = new ArrayList<>();
            parentsSet = false;
        }
    }

    private boolean checkParentTypes(Man father, Woman mother) {
        return father instanceof Man && mother instanceof Woman;
    }

    private void printErrorMessage() {
        if (errorMessage != null) {
            System.out.println(errorMessage);
        }
    }

    public boolean addChild(Human child) {
        if (children.contains(child)) {
            System.out.println("Error: This person is already part of the family.");
            return false;
        }

        if (child.getFamily() == null || child.getFamily().equals(this)) {
            child.setFamily(this);
            children.add(child);
            this.mergeSchedule(child.getSchedule());
            return true;
        } else {
            System.out.println("Error: This person is already part of another family.");
            return false;
        }
    }

    private void mergeSchedule(Map<DayOfWeek, List<String>> childSchedule) {
        for (Map.Entry<DayOfWeek, List<String>> entry : childSchedule.entrySet()) {
            DayOfWeek day = entry.getKey();
            List<String> activities = entry.getValue();

            day.mergeActivities(activities);
        }
    }

    public boolean deleteChild(Human child) {
        if (child == null) {
            System.out.println("Child is null. Cannot delete null child.");
            return false;
        }

        if (children != null) {
            if (children.remove(child)) {
                child.setFamily(null); // Detach child from the family
                System.out.println("Child deleted successfully.");
                System.out.println("Family after deleting child:");
                System.out.println(this);
                return true; // Return true if deletion is successful
            } else {
                System.out.println("Child not found in the family.");
                return false; // Return false if child not found
            }
        }

        return false; // Return false if children is null
    }

    public boolean deleteChild(int index) {
        if (children != null && index >= 0 && index < children.size()) {
            Human childToRemove = children.remove(index);
            if (childToRemove != null) {
                childToRemove.setFamily(null); // Detach child from the family
                System.out.println("Child at index " + index + " deleted successfully.");
                System.out.println("Family after deleting child at index " + index + ":");
                System.out.println(this);
                return true;
            } else {
                System.out.println("Could not delete child at index " + index + ".");
                return false;
            }
        } else {
            System.out.println("Could not delete child at index " + index + ".");
            return false;
        }
    }

    public Human getChild(int index) {
        if (index < 0 || index >= children.size()) {
            throw new IndexOutOfBoundsException("Invalid index: " + index);
        }
        return children.get(index);
    }

    public int countFamily() {
        int parentsCount = 0;

        if (father != null) {
            parentsCount++;
        }

        if (mother != null) {
            parentsCount++;
        }

        return parentsCount + (children != null ? children.size() : 0);
    }

    public Set<Pet> getPets() {
        return new HashSet<>(pets);
    }

    public void setPets(Pet pet) {
        if (this.pets == null) {
            this.pets = new HashSet<>();
        }

        if (pet != null) {
            this.pets.add(pet);
        }
    }

    public Man getFather() {
        return father;
    }

    private void setParent(Human parent, Class<? extends Human> parentClass) {
        if (!parentsSet) {
            if (parentClass.isInstance(parent)) {
                if (parentClass == Man.class) {
                    this.father = (Man) parent;
                } else if (parentClass == Woman.class) {
                    this.mother = (Woman) parent;
                }
                parent.setFamily(this);
                parentsSet = true;
            } else {
                System.out.println("Error: Parent must be an instance of " + parentClass.getSimpleName() + ".");
            }
        } else {
            if ((this.father == null || this.mother == null) && parentClass.isInstance(parent)) {
                if (this.father == null && parentClass == Man.class) {
                    this.father = (Man) parent;
                } else if (this.mother == null && parentClass == Woman.class) {
                    this.mother = (Woman) parent;
                }
                parent.setFamily(this);
            } else {
                System.out.println("Error: Family already has two parents.");
            }
        }
    }

    public void setFather(Human father) {
        setParent(father, Man.class);
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        setParent(mother, Woman.class);
    }

    public List<Human> getChildren() {
        return children != null ? new ArrayList<>(children) : Collections.emptyList();
    }

    public void setChildren(List<Human> newChildren) {
        for (Human child : newChildren) {
            child.setFamily(this);
        }

        children.addAll(newChildren);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;

        Family family = (Family) o;

        boolean parentsEqual = Objects.equals(getFather(), family.getFather()) &&
                Objects.equals(getMother(), family.getMother());

        boolean childrenEqual = getChildren().equals(family.getChildren());

        return parentsEqual && childrenEqual;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getFather(), getMother());

        // Handle the case where children is null
        result = 31 * result + (getChildren() != null ? getChildren().hashCode() : 0);

        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        if (father != null && mother != null) {
            int totalPersons = countFamily();

            if (totalPersons > 0) {
                result.append("Family{\n");
                result.append("  Father: ").append(father).append("\n");
                result.append("  Mother: ").append(mother).append("\n");

                if (children != null && !children.isEmpty()) {
                    List<Human> sortedChildren = new ArrayList<>(children);
                    sortedChildren.sort(Comparator.comparingInt(Human::getYear));
                    result.append("  Children: ").append(sortedChildren).append("\n");
                } else {
                    result.append("  Children: No children in this family.\n");
                }

                if (pets != null && !pets.isEmpty()) {
                    result.append("  Pets: ").append(pets).append("\n");
                } else {
                    result.append("  Pets: No pets in this family.\n");
                }

                result.append("  Total Persons in Family: ").append(totalPersons).append("\n");
                result.append("}");
            }
        }

        return result.toString();
    }
}
