package org.homework7_variant1;

import java.util.*;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<DayOfWeek, List<String>> schedule;
    private Family family;

    static {
        System.out.println("Human class is loaded.");
    }

    {
        System.out.println("A new Human object is created.");
    }

    public Human() {
        this.schedule = new HashMap<>();
    }

    public Human(String name, String surname) {
        this();
        this.name = name;
        this.surname = surname;
        this.schedule = new HashMap<>();
    }

    public Human(String name, String surname, int year) {
        this(name, surname);
        this.year = year;
        this.schedule = new HashMap<>();
    }

    public Human(String name, String surname, int year, Family family) {
        this(name, surname, year);
        this.family = family;
        this.schedule = new HashMap<>();
    }

    public Human(String name, String surname, int year, int iq, Family family, String... schedule) {
        this(name, surname, year, family);
        this.iq = iq;
        this.schedule = sortScheduleByDays(schedule);
    }

    public Human(String name, String surname, int year, int iq, Family family, Map<DayOfWeek, List<String>> schedule) {
        this(name, surname, year, family);
        this.iq = iq;
        this.schedule = sortScheduleByDays(schedule);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIQ() {
        return iq;
    }

    public void setIQ(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, List<String>> getSchedule() {
        return new HashMap<>(schedule);
    }

    private String buildScheduleString(Map<DayOfWeek, List<String>> schedule) {
        StringBuilder scheduleStringBuilder = new StringBuilder("[");
        if (schedule != null && !schedule.isEmpty()) {
            for (Map.Entry<DayOfWeek, List<String>> entry : schedule.entrySet()) {
                scheduleStringBuilder.append(Arrays.toString(new String[]{entry.getKey().name(), String.valueOf(entry.getValue())})).append(", ");
            }
            if (scheduleStringBuilder.length() > 2) {
                scheduleStringBuilder.setLength(scheduleStringBuilder.length() - 2);
            }
        }
        scheduleStringBuilder.append("]");

        return scheduleStringBuilder.toString();
    }

    void setActivitiesForDays(DayOfWeek day, String activity) {
        // If the day is not present in the schedule, add it with a new list of activities
        schedule.computeIfAbsent(day, k -> new ArrayList<>());

        // Add the new activity to the day's list of activities
        schedule.computeIfPresent(day, (k, activities) -> {
            activities.add(activity);
            return activities;
        });
    }

    public void setSchedule(Map<DayOfWeek, List<String>> scheduleMap) {
        if (scheduleMap.isEmpty()) {
            this.schedule = new HashMap<>();
        } else {
            scheduleMap.forEach((day, activities) -> activities.forEach(activity -> setActivitiesForDays(day, activity)));
            this.schedule = new HashMap<>(scheduleMap);
        }
        System.out.println(this.schedule);
    }

    public void setSchedule(String... schedule) {
        this.schedule = sortScheduleByDays(schedule);
        System.out.println(this.schedule);
    }

    public void addDayToSchedule(DayOfWeek day, String activity) {
        // Add the new activity to the day's list of activities
        day.addActivities(activity);

        // Merge the new activity with the existing activities for that day
        List<String> existingActivities = this.schedule.getOrDefault(day, new ArrayList<>());
        existingActivities.add(activity);
        this.schedule.put(day, existingActivities);

        // Print the updated schedule
        System.out.println(this.schedule);
    }

    public Map<DayOfWeek, List<String>> sortScheduleByDays(Map<DayOfWeek, List<String>> schedule) {
        return copyAndSort(schedule);
    }

    private Map<DayOfWeek, List<String>> copyAndSort(Map<DayOfWeek, List<String>> schedule) {
        Map<DayOfWeek, List<String>> sortedSchedule = new TreeMap<>(Comparator.comparingInt(day -> day.ordinal()));

        for (Map.Entry<DayOfWeek, List<String>> entry : schedule.entrySet()) {
            DayOfWeek day = entry.getKey();
            List<String> activities = entry.getValue();
            List<String> dayActivities = sortedSchedule.getOrDefault(day, new ArrayList<>());
            dayActivities.addAll(activities);
            sortedSchedule.put(day, dayActivities);
        }

        return sortedSchedule;
    }

    public Map<DayOfWeek, List<String>> sortScheduleByDays(String... schedule) {
        return copyAndSort(schedule);
    }

    private Map<DayOfWeek, List<String>> copyAndSort(String... schedule) {
        Map<DayOfWeek, List<String>> sortedSchedule = new TreeMap<>(Comparator.comparingInt(Enum::ordinal));

        for (int i = 0; i < schedule.length; i += 2) {
            String dayName = schedule[i];
            String[] activities = schedule[i + 1].split(",\\s*");  // Split activities by comma and optional whitespace

            DayOfWeek day = DayOfWeek.valueOf(dayName);
            List<String> dayActivities = sortedSchedule.getOrDefault(day, new ArrayList<>());
            dayActivities.addAll(Arrays.asList(activities));
            sortedSchedule.put(day, dayActivities);
        }

        return sortedSchedule;
    }

    public Family getFamily() {
        return family;
    }

    public boolean setFamily(Family family) {
        if (this.family != null && !this.family.equals(family)) {
            System.out.println("Error: This human is already part of another family.");
            return false;
        }

        this.family = family;

        return true;
    }

    public String greetPet() {
        if (this.getFamily() != null && this.getFamily().getPets() != null && !this.getFamily().getPets().isEmpty()) {
            StringBuilder greetingBuilder = new StringBuilder("Hello, ");

            for (Pet petMember : this.getFamily().getPets()) {
                greetingBuilder.append(petMember.getNickname()).append("!");
            }

            return greetingBuilder.toString();
        } else {
            return "I don't have a pet.";
        }
    }

    public String describePet() {
        StringBuilder resultBuilder = new StringBuilder();
        if (this.family != null && this.family.getPets() != null && !this.family.getPets().isEmpty()) {
            for (Pet petMember : family.getPets()) {
                Species species = petMember.getSpecies();
                int age = petMember.getAge();
                int cunningLevel = petMember.getTrickLevel();

                String cunningDescription = (cunningLevel > 50) ? "very cunning" : "almost not cunning";

                resultBuilder.append("I have a ").append(species).append(". It is ").append(age)
                        .append(" years old, and it is ").append(cunningDescription).append(".");
            }
        } else {
            resultBuilder.append("I don't have a pet.");
        }
        return resultBuilder.toString().trim();
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (this.family != null && this.family.getPets() != null) {
            Set<Pet> pets = this.family.getPets();

            if (!pets.isEmpty()) {
                Pet petToFeed = choosePetToFeed(pets);

                if (isTimeToFeed) {
                    System.out.println("Hmm... I will feed " + petToFeed.getNickname());
                    return true;
                } else {
                    Random random = new Random();

                    int randomTrick = random.nextInt(101);
                    int petTrickLevel = petToFeed.getTrickLevel();
                    System.out.println("Random Trick: " + randomTrick);
                    System.out.println("Pet Trick: " + petTrickLevel);

                    if (petTrickLevel > randomTrick) {
                        System.out.println("Hmm... I will feed " + petToFeed.getNickname());
                        return true;
                    } else {
                        System.out.println("I think " + petToFeed.getNickname() + " is not hungry.");
                        return false;
                    }
                }
            } else {
                System.out.println("I don't have a pet.");
                return false;
            }
        } else {
            System.out.println("I don't have a pet.");
            return false;
        }
    }

    private Pet choosePetToFeed(Set<Pet> pets) {
        return pets.stream().findFirst().orElse(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        return getYear() == human.getYear() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getYear());
    }

    @Override
    public String toString() {
        this.schedule = sortScheduleByDays(schedule);
        String scheduleString = buildScheduleString(schedule);
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + scheduleString +
                '}';
    }
}
