package org.homework7_variant1;

import java.util.*;

public abstract class Pet {
    private int age;
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int trickLevel;
    private Set<String> habits;

    static {
        System.out.println("Pet class is loaded.");
    }

    {
        System.out.println("A new Pet object is created.");
    }

    // Empty constructor
    public Pet() {
        habits = new HashSet<>();
    }

    public Pet(String nickname) {
        this.nickname = nickname;
        habits = new TreeSet<>();
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = new TreeSet<>(habits);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public abstract void respond();

    public abstract void eat();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;

        Pet pet = (Pet) o;

        return getAge() == pet.getAge() &&
                Objects.equals(getNickname(), pet.getNickname()) &&
                Objects.equals(getSpecies(), pet.getSpecies());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAge(), getNickname(), getSpecies());
    }

    @Override
    public String toString() {
        return String.format("Pet{%n" +
                        "    species=%s%n" +
                        "    canFly=%s%n" +
                        "    hasFur=%s%n" +
                        "    numberOfLegs=%d%n" +
                        "    nickname=%s%n" +
                        "    age=%d%n" +
                        "    trickLevel=%d%n" +
                        "    habits=%s%n" +
                        "}",
                species != null ? species : Species.UNKNOWN,
                species.canFly(),
                species.hasFur(),
                species.getNumberOfLegs(),
                nickname != null ? nickname : "null",
                age,
                trickLevel,
                habits != null ? habits.toString() : "null");
    }
}
