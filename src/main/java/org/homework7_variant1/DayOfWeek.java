package org.homework7_variant1;

import java.util.*;

public enum DayOfWeek {
    MONDAY(new ArrayList<>()),
    TUESDAY(new ArrayList<>()),
    WEDNESDAY(new ArrayList<>()),
    THURSDAY(new ArrayList<>()),
    FRIDAY(new ArrayList<>()),
    SATURDAY(new ArrayList<>()),
    SUNDAY(new ArrayList<>());

    private List<String> activities;

    DayOfWeek(List<String> activities) {
        this.activities = activities;
    }

    public void addActivities(String activity) {
        this.activities.add(activity);
    }

    public void mergeActivities(List<String> newActivities) {
        Set<String> mergedSet = new HashSet<>(this.activities);
        mergedSet.addAll(newActivities);
        this.activities = new ArrayList<>(mergedSet);
    }

    public static List<String> getActivitiesForDay(DayOfWeek day, Map<DayOfWeek, ?> schedule) {
        Object activities = schedule.get(day);

        if (activities != null) {
            if (activities instanceof List<?>) {
                return new ArrayList<>((List<String>) activities);
            } else if (activities instanceof String) {
                return Collections.singletonList((String) activities);
            }
        }

        return Collections.singletonList("No activity specified");
    }

    public static String getActivityForDay(DayOfWeek dayOfWeek, Map<DayOfWeek, List<String>> schedule) {
        List<String> activities = schedule.getOrDefault(dayOfWeek, Collections.emptyList());

        return activities.isEmpty() ?
                "No activity specified" :
                String.join(", ", activities);
    }

    public void setActivities(List<String> activities) {
        this.activities.addAll(activities);
    }
}
