package org.homework7_variant1;

public interface HumanCreator {

    Human bornChild(String childName, String fatherSurname, Family family, int averageIQ);
}
