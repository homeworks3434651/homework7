package homework7_variant1_Test;

import org.homework7_variant1.*;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class FamilyTest_hamcrest {

    Family family = TestUtilities.setUpFamily();

    @Test
    public void testAddChildToFamily() {
        // Arrange
        Family family = TestUtilities.setUpFamily();
        Human child = new Human("Bob", "Doe", 1995);

        // Act
        boolean addChildResult = family.addChild(child);

        // Assert
        assertThat("Adding a child to the family should return true", addChildResult, is(true));
        assertThat("The family should contain the added child", family.getChildren(), hasItem(child));
        assertThat("The number of children in the family should be 1", family.getChildren(), hasSize(1));
        assertThat("The added child should be the specified child", family.getChildren().get(0), sameInstance(child));
        assertThat("The family reference of the child should be the family", child.getFamily(), sameInstance(family));
    }

    @Test
    public void addChildShouldNotAddDuplicateChild() {
        Human child = new Human("Alex", "Doe", 2000);

        assertThat("Adding the same child should return true the first time", family.addChild(child), is(true));
        assertThat("Adding the same child again should return false", family.addChild(child), is(false));
    }

    @Test
    public void deleteChildShouldRemoveChildFromFamily() {
        // Arrange
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat("Adding child1 to the family should return true", family.addChild(child1), is(true));
        assertThat("Adding child2 to the family should return true", family.addChild(child2), is(true));

        // Act
        family.deleteChild(child1);

        // Assert
        assertThat("The family should not contain child1", family.getChildren(), not(hasItem(child1)));

        // Check that the array remains unchanged if a non-equivalent object is passed
        Human nonExistentChild = new Human("Non", "Existent", 1998);
        int originalChildrenCount = family.getChildren().size();

        // Act
        family.deleteChild(nonExistentChild);

        // Assert
        assertThat("The number of children in the family should remain unchanged",
                family.getChildren(), hasSize(originalChildrenCount));
    }

    @Test
    public void deleteChildByIndexShouldRemoveChildFromFamily() {
        // Arrange
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat("Adding child1 to the family should return true", family.addChild(child1), is(true));
        assertThat("Adding child2 to the family should return true", family.addChild(child2), is(true));

        // Act
        boolean result = family.deleteChild(0); // Assuming 0 is the index of child1

        // Assert
        assertThat("Deleting child1 by index should return true", result, is(true));
        assertThat("The family should not contain child1", family.getChildren(), not(hasItem(child1)));

        // Check that the array remains unchanged if an out-of-range index is passed
        result = family.deleteChild(2); // Assuming an out-of-range index

        // Assert
        assertThat("Deleting child by out-of-range index should return false", result, is(false));
        assertThat("The number of children in the family should remain unchanged",
                family.getChildren(), hasSize(1));
    }

    @Test
    public void testCountFamily() {
        // Arrange
        assertThat("The initial family should have a count of 2", family.countFamily(), is(equalTo(2)));

        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat("Adding child1 to the family should return true", family.addChild(child1), is(true));
        assertThat("Adding child2 to the family should return true", family.addChild(child2), is(true));

        // Act & Assert
        assertThat("The family count after adding two children should be 4",
                family.countFamily(), is(equalTo(4)));

        // Act
        family.deleteChild(child1);

        // Assert
        assertThat("The family count after deleting one child should be 3",
                family.countFamily(), is(equalTo(3)));
    }

    @Test
    public void testSetFather_and_Mother() {
        // Arrange
        Family family1 = new Family(null, null);
        Man father = new Man("John", "Doe", 1970);
        family1.setFather(father);

        // Assert
        assertThat("The father of family1 should be John Doe", family1.getFather(), is(equalTo(father)));
        assertThat("The family of John Doe should be family1", father.getFamily(), is(equalTo(family1)));

        Woman mother = new Woman("Jane", "Doe", 1975);
        family1.setMother(mother);

        Man newFather = new Man("Bob", "Kennedy", 1970);
        family1.setFather(newFather);

        // Assert that the third parent is not set due to the error
        assertThat("The family of newFather should be null", newFather.getFamily(), is(nullValue()));

        Human newMother = new Human("Marina", "Krotova", 1980);
        family1.setMother(newMother);

        // Assert that the third parent is not set due to the error
        assertThat("The family of newMother should be null", newMother.getFamily(), is(nullValue()));
    }

    @Test
    public void testEqualsAndHashCode() {
        Man father2 = new Man("John", "Doe", 1970);
        Woman mother2 = new Woman("Jane", "Doe", 1975);
        Family family2 = new Family(father2, mother2);
        Man father3 = new Man("Charlie", "Brown", 1990);
        Woman mother3 = new Woman("Lucy", "Brown", 1992);
        Family family3 = new Family(father3, mother3);

        // Testing reflexivity
        assertThat("Family should be equal to itself", family, is(equalTo(family)));

        // Testing consistency
        assertThat("Family should be equal to family2", family, is(equalTo(family2)));

        // Testing symmetry
        assertThat("Family2 should be equal to family", family2, is(equalTo(family)));

        // Testing transitivity
        assertThat("Family should be equal to family2", family, is(equalTo(family2)));
        assertThat("Family2 should be equal to family", family2, is(equalTo(family)));
        assertThat("Family should be equal to family2", family, is(equalTo(family2)));

        // Testing equality with null
        assertThat("Family should not be equal to null", family, is(not(equalTo(null))));

        // Testing hash code consistency
        assertThat("Hash codes of family and family2 should be equal",
                family.hashCode(), is(equalTo(family2.hashCode())));

        // Testing hash code inequality with different objects
        assertThat("Hash codes of family and family3 should not be equal",
                family.hashCode(), is(not(equalTo(family3.hashCode()))));
        assertThat("Family should be equal to family2", family, is(equalTo(family2)));

        // Override hashCode for family1 to introduce a discrepancy
        family2 = new Family(father2, mother2) {
            @Override
            public int hashCode() {
                return Objects.hash(getFather(), getMother());
            }
        };

        assertThat("Family should not be equal to family2",
                family, is(not(equalTo(family2))));
        assertThat("Hash codes of family and family2 should not be equal",
                family.hashCode(), is(not(equalTo(family2.hashCode()))));

        Man father4 = new Man("Charlie", "Brown", 1990);
        Woman mother4 = new Woman("Lucy", "Brown", 1992);
        Family family4 = new Family(father4, mother4);
        Human sonFamily3 = new Human("Bryan", "Brown", 2010);
        family4.addChild(sonFamily3);

        // Update: Using getChildren instead of Arrays.asList for correct comparison
        family4.setChildren(List.of(sonFamily3));

        assertThat("Family3 should not be equal to family4",
                family3, is(not(equalTo(family4))));
    }

    @Test
    public void testSetChildren() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        // Act
        family.setChildren(List.of(child1, child2));

        // Assert
        assertThat("Family should contain child1", family.getChildren(), hasItem(child1));
        assertThat("Family should contain child2", family.getChildren(), hasItem(child2));

        assertThat("Child1 should have family reference set to family", child1.getFamily(), is(equalTo(family)));
        assertThat("Child2 should have family reference set to family", child2.getFamily(), is(equalTo(family)));
    }

    @Test
    public void setChildrenShouldUpdateExistingChildrenList() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);
        family.addChild(child1);
        family.setChildren(List.of(child2));

        // Assert
        assertThat("Family should contain child1", family.getChildren(), hasItem(child1));
        assertThat("Family should contain child2", family.getChildren(), hasItem(child2));

        assertThat("Child2 should have family reference set to family", child2.getFamily(), is(equalTo(family)));
        assertThat("Child1 should still have family reference set to family", child1.getFamily(), is(equalTo(family)));
    }

    @Test
    public void setChildrenShouldHandleEmptyList() {
        // Act
        family.setChildren(Collections.emptyList());

        // Assert
        assertThat("Family's children list should be empty", family.getChildren(), is(empty()));
    }

    @Test
    public void testToString() {
        Human child1 = new Human("Bob", "Doe", 2005);
        Human child2 = new Human("Alice", "Doe", 2010);
        family.addChild(child1);
        family.addChild(child2);

        Pet myPet = new Dog(); // Assuming Dog is a subclass of Pet
        myPet.setNickname("Buddy");
        family.setPets(myPet);

        // Act
        String familyString = family.toString();

        // Assert
        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: [" + child1 + ", " + child2 + "]\n" +
                "  Pets: [" + myPet + "]\n" +  // Represent Pets as a list
                "  Total Persons in Family: 4\n" +
                "}";
        assertThat("Family toString() should match the expected output", familyString, is(equalTo(expectedOutput)));
    }

    @Test
    public void testToStringWithNoChildrenAndNoPet() {
        // Act
        String familyString = family.toString();

        // Assert
        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: No children in this family.\n" +
                "  Pets: No pets in this family.\n" +
                "  Total Persons in Family: 2\n" +
                "}";
        assertThat("Family toString() should match the expected output", familyString, is(equalTo(expectedOutput)));
    }
}
