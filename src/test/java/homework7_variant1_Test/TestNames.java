package homework7_variant1_Test;

public class TestNames {

    public static boolean isInArray(String value, String[] array) {
        for (String element : array) {
            if (element.equals(value)) {
                return true;
            }
        }
        return false;
    }
}
