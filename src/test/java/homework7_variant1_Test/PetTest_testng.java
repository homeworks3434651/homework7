package homework7_variant1_Test;

import org.homework7_variant1.*;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static org.testng.Assert.*;

public class PetTest_testng {

    @Test
    public void testEat() {
        Dog pet = new Dog();

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        pet.eat();

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        String expectedOutput = "I eat meat.";
        String actualOutput = outContent.toString().trim();

        assertEquals(actualOutput, expectedOutput);
    }

    @Test
    public void testRespond() {
        Dog pet = new Dog();
        pet.setNickname("TestPet");

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        pet.respond();

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        String expectedOutput = "Woof! I'm a dog.";
        String actualOutput = outContent.toString().trim();

        assertEquals(actualOutput, expectedOutput);
    }

    @Test
    public void testFoul() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        Dog pet = new Dog();
        pet.foul();

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        String expectedOutput = "I left a little surprise for you on the carpet.";
        String actualOutput = outContent.toString().trim();

        assertTrue(actualOutput.contains(expectedOutput));
    }

    @Test
    public void testEquals_and_hashCode() {
        Set<String> habits1 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits2 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits3 = new HashSet<>(Set.of("play", "rollover"));

        DomesticCat pet1 = new DomesticCat("Whiskers", 3, 50, habits1);
        DomesticCat pet2 = new DomesticCat("Whiskers", 3, 50, habits2);
        DomesticCat pet3 = new DomesticCat("Thomas", 2, 60, habits3);

        // Testing reflexivity
        assertEquals(pet1, pet1);

        // Testing consistency
        assertEquals(pet1, pet2);

        // Testing symmetry
        assertEquals(pet2, pet1);

        // Testing transitivity
        assertEquals(pet1, pet2);
        assertEquals(pet2, pet1);
        assertEquals(pet1, pet2);

        // Testing equality with null
        assertNotEquals(pet1, null);

        // Testing hash code consistency
        assertEquals(pet1.hashCode(), pet2.hashCode());

        // Testing hash code inequality with different objects
        assertNotEquals(pet1.hashCode(), pet3.hashCode());

        // Override hashCode for pet1 to introduce a discrepancy
        pet1 = new DomesticCat("Whiskers", 3, 50, habits1) {
            @Override
            public int hashCode() {
                return Objects.hash(getAge());
            }
        };
        assertEquals(pet1, pet2);
        assertNotEquals(pet1.hashCode(), pet2.hashCode());

        // Changing a field after object creation
        pet1.setAge(4);
        assertNotEquals(pet1, pet2);
    }

    @Test
    public void testEqualsWhenSpeciesIsUnknown() {
        Set<String> habits1 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits2 = new HashSet<>(Set.of("play", "sleep"));

        // Create two Pet objects with UNKNOWN species
        Pet pet1 = new TestPet("Buddy", 3, 50, habits1);
        Pet pet2 = new TestPet("Buddy", 3, 50, habits2);

        // Ensure the equals method works as expected
        assertEquals(pet1, pet2);
        assertEquals(pet2, pet1);
    }

    @Test
    public void toStringShouldReturnFormattedString() {
        Dog dog = new Dog();

        String expected1 = String.format("Pet{%n" +
                        "    species=DOG%n" +
                        "    canFly=false%n" +
                        "    hasFur=true%n" +
                        "    numberOfLegs=4%n" +
                        "    nickname=%s%n" +
                        "    age=0%n" +
                        "    trickLevel=0%n" +
                        "    habits=%s%n" +
                        "}",
                (String) null,
                "[]");

        String actual1 = dog.toString();

        assertEquals(actual1, expected1);

        Fish pet = new Fish();

        String expected2 = String.format("Pet{%n" +
                        "    species=FISH%n" +
                        "    canFly=false%n" +
                        "    hasFur=false%n" +
                        "    numberOfLegs=0%n" +
                        "    nickname=%s%n" +
                        "    age=0%n" +
                        "    trickLevel=0%n" +
                        "    habits=%s%n" +
                        "}",
                (String) null,
                "[]");

        String actual2 = pet.toString();

        assertEquals(actual2, expected2);
    }

    @Test
    public void testToStringWhenSpeciesIsUnknown() {
        Set<String> habits = new HashSet<>(Set.of("play", "sleep"));

        // Create a Pet object with UNKNOWN species
        Pet pet = new TestPet("Buddy", 3, 50, habits);

        // Define the expected output
        String expectedOutput = String.format("Pet{%n" +
                        "    species=%s%n" +
                        "    canFly=%s%n" +
                        "    hasFur=%s%n" +
                        "    numberOfLegs=%d%n" +
                        "    nickname=%s%n" +
                        "    age=%d%n" +
                        "    trickLevel=%d%n" +
                        "    habits=%s%n" +
                        "}",
                Species.UNKNOWN,
                Species.UNKNOWN.canFly(),
                Species.UNKNOWN.hasFur(),
                Species.UNKNOWN.getNumberOfLegs(),
                "Buddy",
                3,
                50,
                "[play, sleep]");

        // Ensure the toString method works as expected
        assertEquals(pet.toString(), expectedOutput);
    }
}
