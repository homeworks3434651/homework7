package homework7_variant1_Test;

import org.homework7_variant1.*;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

public class HumanTest_assertj {

    Family family = TestUtilities.setUpFamily();

    @Test
    void testSetFamily() {
        Human human3 = new Human("Alex", "Doe", 2000);

        assertThat(human3.setFamily(family)).isTrue();
        assertThat(human3.getFamily()).isEqualTo(family);

        Family family2 = new Family(new Man("Bob", "Smith", 1990), new Woman("Alice", "Smith", 1995));

        assertThat(human3.setFamily(family2)).isFalse();
        assertThat(human3.getFamily()).isEqualTo(family); // Family should remain unchanged
    }

    @Test
    void testFeedPetWhenTimeToFeed() {
        Dog pet = new Dog(); // Assuming Dog is a subclass of Pet
        pet.setNickname("Buddy");
        pet.setTrickLevel(52);

        family.setPets(pet);

        // Test feeding when it's time to feed
        assertThat(family.getFather().feedPet(true)).isTrue();
    }

    @Test
    void testFeedPet_shouldReturnTrueOrFalse() {
        DomesticCat cat = new DomesticCat("Whiskers");
        // Assuming setTrickLevel generates a random value between 0 and 100
        cat.setTrickLevel(50);

        family.setPets(cat);

        // Now, instead of expecting a specific result, you can assert that it's either true or false
        assertThat(family.getFather().feedPet(false)).isIn(true, false);
    }

    @Test
    void testFeedPet() {
        // Arrange
        DomesticCat cat = new DomesticCat("Whiskers");
        Family family = new Family(new Man("John", "Doe", 1970), new Woman("Jane", "Doe", 1975));
        family.setPets(cat);
        family.getFather().setFamily(family);

        // Retrieve the specific pet instance from the set
        Pet specificPet = family.getPets().iterator().next();

        // Test when it's not time to feed, and pet trick level is higher
        specificPet.setTrickLevel(100);
        assertThat(family.getFather().feedPet(false)).isTrue();

        // Test when it's not time to feed, and pet trick level is small
        specificPet.setTrickLevel(0);
        assertThat(family.getFather().feedPet(false)).isFalse();
    }

    @Test
    void testGreetPet() {
        Human child = new Human("Alex", "Doe", 2000);
        family.addChild(child);

        DomesticCat cat = new DomesticCat("Whiskers");
        family.setPets(cat);

        assertThat(family.getMother().greetPet()).isEqualTo("Hello, I greet you Whiskers!");
        assertThat(family.getFather().greetPet()).isEqualTo("Hello, my favorite friend Whiskers!");
        assertThat(child.greetPet()).isEqualTo("Hello, Whiskers!");

        // Test greeting when there is no pet
        Family newFamily = new Family(new Man("John", "Smith", 1970), new Woman("Jane", "Smith", 1975));
        Human childNewFamily = new Human("Teresa", "Smith", 2000);
        newFamily.addChild(childNewFamily);
        newFamily.setPets(null);
        assertThat(newFamily.getMother().greetPet()).isEqualTo("I don't have a pet.");
        assertThat(newFamily.getFather().greetPet()).isEqualTo("I don't have a pet.");
        assertThat(childNewFamily.greetPet()).isEqualTo("I don't have a pet.");
    }

    @Test
    void testDescribePet() {
        Dog pet = new Dog(); // Assuming Dog is a subclass of Pet
        pet.setNickname("Buddy");
        pet.setAge(3);
        pet.setTrickLevel(52);
        family.setPets(pet);

        // Test describing the pet
        assertThat(family.getMother().describePet())
                .isEqualTo("I have a DOG. It is 3 years old, and it is very cunning.");

        // Test describing when there is no pet
        Family newFamily = new Family(new Man("John", "Smith", 1970), new Woman("Jane", "Smith", 1975));
        Human childNewFamily = new Human("Teresa", "Smith", 2000);
        newFamily.addChild(childNewFamily);
        newFamily.setPets(null);

        assertThat(newFamily.getMother().describePet()).isEqualTo("I don't have a pet.");
        assertThat(newFamily.getFather().describePet()).isEqualTo("I don't have a pet.");
        assertThat(childNewFamily.describePet()).isEqualTo("I don't have a pet.");
    }

    @Test
    void testRepairCar() {
        Man man = new Man("John", "Doe", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the respond() method
        man.repairCar();

        // Reset the standard output stream
        System.setOut(new PrintStream(new ByteArrayOutputStream()));

        // Check only the output related to the respond() method
        assertThat(outputStreamCaptor.toString().trim()).isEqualTo("I'm repairing the car.");
    }

    @Test
    void testMakeUp() {
        Woman woman = new Woman("Alice", "Smith", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the respond() method
        woman.makeup();

        // Reset the standard output stream
        System.setOut(new PrintStream(new ByteArrayOutputStream()));

        // Check only the output related to the respond() method
        assertThat(outputStreamCaptor.toString().trim()).isEqualTo("I'm applying makeup.");
    }

    @Test
    public void testBornChildWithNullName() {
        family.getFather().setIQ(125);
        family.getMother().setIQ(105);

        Human child = family.getMother().bornChild(null, "Doe", family, 115);

        assertThat(child).isNotNull();
        assertThat(child.getName()).isNotNull().isNotEmpty();
        assertThat(child.getSurname()).isEqualTo("Doe");
        assertThat(child.getFamily()).isEqualTo(family);

        int inheritedIQ = family.getMother().calculateInheritedIQ(family.getFather(), family.getMother(), child);
        assertThat(child.getIQ()).isEqualTo(inheritedIQ);

        assertThat(child).isInstanceOfAny(Man.class, Woman.class);

        String randomMaleName = family.getMother().getRandomMaleName();
        assertThat(randomMaleName).isNotNull();
        assertThat(family.getMother().MALE_NAMES).contains(randomMaleName);

        String randomFemaleName = family.getMother().getRandomFemaleName();
        assertThat(randomFemaleName).isNotNull();
        assertThat(family.getMother().FEMALE_NAMES).contains(randomFemaleName);
    }

    @Test
    void testEqualsAndHashCode() {
        Man human1 = new Man("John", "Doe", 1980);
        Man human2 = new Man("John", "Doe", 1980);
        Man human3 = new Man("Jane", "Doe", 1985);

        assertThat(human1).isEqualTo(human2).isNotEqualTo(human3);
        assertThat(human1.hashCode()).isEqualTo(human2.hashCode()).isNotEqualTo(human3.hashCode());
    }

    @Test
    public void testToString() {
        Family family = new Family(new Man("Charlie", "Brown", 1990),
                new Woman("Lucy", "Brown", 1992));

        Human human = new Human("Bryan", "Brown", 2010, 120, family,
                DayOfWeek.TUESDAY.name(), "Studying", DayOfWeek.SATURDAY.name(), "Gym");

        // Sort the schedule before constructing the expected output
        human.setSchedule(human.sortScheduleByDays(human.getSchedule()));

        String expectedOutput = "Human{name='Bryan', surname='Brown', year=2010, iq=120, schedule=[[TUESDAY, [Studying]], [SATURDAY, [Gym]]]}";
        assertThat(human).hasToString(expectedOutput);

        Man man = new Man("John", "Doe", 1980);
        man.setIQ(120);

        // Sort the schedule before constructing the expected output
        man.setSchedule(man.sortScheduleByDays(man.getSchedule()));

        // Test the string representation of the man
        expectedOutput = "Man{name='John', surname='Doe', year=1980, iq=120, schedule={}}";
        assertThat(man).hasToString(expectedOutput);

        Woman woman = new Woman("Alice", "Smith", 1980);

        // Sort the schedule before constructing the expected output
        woman.setSchedule(woman.sortScheduleByDays(woman.getSchedule()));

        // Test the string representation of the woman
        expectedOutput = "Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule={}}";
        assertThat(woman).hasToString(expectedOutput);
    }
}
