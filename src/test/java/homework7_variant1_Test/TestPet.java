package homework7_variant1_Test;

import org.homework7_variant1.Pet;
import org.homework7_variant1.Species;

import java.util.Set;

public class TestPet extends Pet {
    public TestPet(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.UNKNOWN);
    }

    @Override
    public void respond() {
        // Implement abstract method
    }

    @Override
    public void eat() {
        // Implement abstract method
    }
}
