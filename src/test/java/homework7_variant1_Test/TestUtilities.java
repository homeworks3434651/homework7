package homework7_variant1_Test;

import org.homework7_variant1.Family;
import org.homework7_variant1.Man;
import org.homework7_variant1.Woman;

public class TestUtilities {

    public static Family setUpFamily() {
        Man father = new Man("John", "Doe", 1970);
        Woman mother = new Woman("Jane", "Doe", 1975);
        Family family = new Family(father, mother);
        return family;
    }
}
